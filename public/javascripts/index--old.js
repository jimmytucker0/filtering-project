var placeholderData = [
  {
      id: 0,
      text: 'enhancement'
  },
  {
      id: 1,
      text: 'bug'
  },
  {
      id: 2,
      text: 'duplicate'
  },
  {
      id: 3,
      text: 'invalid'
  },
  {
      id: 4,
      text: 'wontfix'
  }
];

let allVenues = [];
const initialize = async () => {
  const response = await fetch("/venues/options");
  const jsonData = await response.json();
  console.log(jsonData);
}
$(function() {
  console.log( "ready!" );
  let lobInput = $('#lob-select-field');
  let eventsInput = $('#events-select-field');
  let venuesFiltersInput = $('#venue-filters-select-field');
  let venuesInput = $('#venues-select-field');
  let deliverInput = $('#deliver-select-field');

  lobInput.select2({
    theme: 'bootstrap-5',
    minimumResultsForSearch: Infinity,
    ajax: {
      url: '/venues/options',
      processResults: function (data) {
        console.log('data', data);
        // Transforms the top-level key of the response object from 'items' to 'results'
        let mappedData = data.lob.map(el => {
          el.text = el.value;
          return el;
        })
        return {
          results: mappedData
        };
      }
    }
  });

  eventsInput.select2({
    theme: 'bootstrap-5',
    ajax: {
      url: '/venues/options',
      processResults: function (data) {
        console.log('data', data);
        // Transforms the top-level key of the response object from 'items' to 'results'
        let mappedData = data.events.map(el => {
          el.text = el.value;
          return el;
        })
        return {
          results: mappedData
        };
      }
    }
  });

  venuesFiltersInput.select2({
    theme: 'bootstrap-5',
    minimumResultsForSearch: -1,
    ajax: {
      url: '/venues/options',
      processResults: function (data) {
        console.log('data', data);
        // Transforms the top-level key of the response object from 'items' to 'results'
        let mappedData = data.venueFilters.map(el => {
          el.text = el.value;
          return el;
        })
        return {
          results: mappedData
        };
      }
    }
  });

  venuesInput.select2({
    theme: 'bootstrap-5',
    minimumResultsForSearch: 3,
    ajax: {
      url: '/venues/options',
      processResults: function (data) {
        console.log('data', data);
        // Transforms the top-level key of the response object from 'items' to 'results'
        let mappedData = data.venues.map(el => {
          el.text = el.value;
          return el;
        })
        allVenues = mappedData;
        console.log('venuesPopulated allVenues', allVenues);
        return {
          results: mappedData
        };
      }
    }
  });

  deliverInput.select2({
    maximumSelectionLength: 2,
    theme: 'bootstrap-5',
    ajax: {
      url: '/venues/options',
      processResults: function (data) {
        console.log('data', data);
        // Transforms the top-level key of the response object from 'items' to 'results'
        let mappedData = data.deliverTo.map(el => {
          el.text = el.value;
          return el;
        })
        return {
          results: mappedData
        };
      }
    }
  });

  venuesFiltersInput.on('select2:select', function (e) {
      var data = e.params.data;
      console.log(data);

      console.log('allVenues', allVenues);
      console.log(`allVenues.filter(el => {
        return el.tags.includes(data.id)
      })`, allVenues.filter(el => {
        return el.tags.includes(data.id)
      }))

  
      venuesInput.select2({
        theme: 'bootstrap-5',
        data: allVenues.filter(el => {
          return el.tags.includes(data.id)
        })
      })
    });

  // lobInput.on('select2:select', function (e) {
  //   var data = e.params.data;
  //   console.log(data);

  //   eventsInput.select2({
  //     theme: 'bootstrap-5',
  //     data: placeholderData
  //   })
  // });

});