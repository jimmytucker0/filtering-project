// helper fn
const mapDataBeforeRender = (responseArray = []) => responseArray.map(el => {
  el.text = el.value
  return el
});

const INPUT_KEYS = [
  'lob',
  'events',
  'venueFilters',
  'venues',
  'deliverTo',
];

// this is going to store all the inputs with all the value
// fetch once, then reference this
let allInputConfigs = {
  
};
// this is going to be the current state
// filter out venues for certain venueFilters
// filter our deliverTo when a certain venue and LOB is selected etc.
let allInputState = {
  lob: [],
  events: [],
  venueFilters: [],
  venues: [],
  deliverTo: [],
}

// use these to update filters
let allSelectedInputs = {
  lob: [],
  events: [],
  venueFilters: [],
  venues: [],
  deliverTo: [],
}

// {
//   inputDataKey: 'deliverTo',
//   inputDOMNode: nodeFromJQuery,
//   inputDOMNodeParams: object with extra key/vals to include
// }
let inputNodes = [];

const refreshAllInputs = () => inputNodes.forEach(
  ({ 
    inputDataKey, 
    inputDOMNode,
    inputDOMNodeParams
  }) => {
    const stateBeforeDestroy = inputDOMNode.select2('data');
    inputDOMNode.select2('destroy');
    inputDOMNode.empty();
    let willBeNull = true;
    inputDOMNode.select2({
      theme: 'bootstrap-5',
      ...inputDOMNodeParams,
      data: mapDataBeforeRender(allInputState[inputDataKey]).map(el => {
        el.selected = false;
        stateBeforeDestroy.forEach(innerEl => {
          if (!innerEl.id || innerEl.id !== el.id) return;
          if (innerEl.selected) {
            el.selected = true;
            willBeNull = false;
          }
        })
        return el;
      })
    });
    if (willBeNull) {
      inputDOMNode.val('null').trigger('change');
    }
  }
)

const filterAllAfterChange = ({
  inputKey,
  inputValue,
}) => {
  let arrayCopy = [];
  arrayCopy = [...allInputState.lob]
  allInputState.lob = arrayCopy.filter((el => {
    const toRemove = el.removeWhen[inputKey].includes(inputValue)
    return !toRemove
  }))
  arrayCopy = [...allInputState.events]
  allInputState.events = arrayCopy.filter((el => {
    const toRemove = el.removeWhen[inputKey].includes(inputValue)
    return !toRemove
  }))
  arrayCopy = [...allInputState.venueFilters]
  allInputState.venueFilters = arrayCopy.filter((el => {
    const toRemove = el.removeWhen[inputKey].includes(inputValue)
    return !toRemove
  }))
  arrayCopy = [...allInputState.venues]
  allInputState.venues = arrayCopy.filter((el => {
    const toRemove = el.removeWhen[inputKey].includes(inputValue)
    return !toRemove
  }))
  arrayCopy = [...allInputState.deliverTo]
  allInputState.deliverTo = arrayCopy.filter((el => {
    const toRemove = el.removeWhen[inputKey].includes(inputValue)
    return !toRemove
  }))

  refreshAllInputs();
}

const initializeInputs = async ({
  lobInput = null,
  eventsInput = null,
  venueFiltersInput = null,
  venuesInput = null,
  deliverInput = null,
}) => {
  const response = await fetch("/venues/options");
  const jsonData = await response.json();
  // keep this obj
  allInputConfigs = Object.assign({}, jsonData);
  // use this when filtering
  allInputState = Object.assign({}, jsonData);

  lobInput.select2({
    theme: 'bootstrap-5',
    minimumResultsForSearch: -1,
    data: mapDataBeforeRender(allInputState.lob)
  });

  eventsInput.select2({
    theme: 'bootstrap-5',
    data: mapDataBeforeRender(allInputState.events)
  });

  venueFiltersInput.select2({
    theme: 'bootstrap-5',
    minimumResultsForSearch: -1,
    data: mapDataBeforeRender(allInputState.venueFilters)
  });

  venuesInput.select2({
    theme: 'bootstrap-5',
    minimumResultsForSearch: 5,
    data: mapDataBeforeRender(allInputState.venues)
  });

  deliverInput.select2({
    maximumSelectionLength: 2,
    theme: 'bootstrap-5',
    data: mapDataBeforeRender(allInputState.deliverTo)
  });
}

$(function() {
  $("#openers-select-field").select2({
    theme: 'bootstrap-5',
    tags: true,
    // tokenSeparators: [',', ' '],
    maximumSelectionLength: 50, 
    multiple: true,
  });
  
  let lobInput = $('#lob-select-field');
  inputNodes.push({
    inputDataKey: 'lob',
    inputDOMNode: lobInput,
    inputDOMNodeParams: { minimumResultsForSearch: -1 }
  })
  let eventsInput = $('#events-select-field');
  inputNodes.push({
    inputDataKey: 'events',
    inputDOMNode: eventsInput,
    inputDOMNodeParams: {}
  })
  let venueFiltersInput = $('#venue-filters-select-field');
  inputNodes.push({
    inputDataKey: 'venueFilters',
    inputDOMNode: venueFiltersInput,
    inputDOMNodeParams: { minimumResultsForSearch: -1 }
  })
  let venuesInput = $('#venues-select-field');
  inputNodes.push({
    inputDataKey: 'venues',
    inputDOMNode: venuesInput,
    inputDOMNodeParams: { minimumResultsForSearch: 5 }
  })
  let deliverInput = $('#deliver-select-field');
  inputNodes.push({
    inputDataKey: 'deliverTo',
    inputDOMNode: deliverInput,
    inputDOMNodeParams: { maximumSelectionLength: 5, multiple: true }
  })

  const forceSync = () => {
    allInputState = {
      ...allInputConfigs
    }
    
    inputNodes.forEach(el => {
      let currentInputValue = el.inputDOMNode.val();
      if (Array.isArray(currentInputValue)) {
        // this is an input where u can have multiple selected
        currentInputValue.forEach(thisId => {
          filterAllAfterChange({
            inputKey: el.inputDataKey,
            inputValue: thisId,
          });
        })
      } else {
        filterAllAfterChange({
          inputKey: el.inputDataKey,
          inputValue: currentInputValue,
        });
      }
    });
  }

  lobInput.on('select2:select', function (e) {
    forceSync();
  });

  eventsInput.on('select2:select', function (e) {
    forceSync();
  });

  venueFiltersInput.on('select2:select', function (e) {
    forceSync();
  });

  venuesInput.on('select2:select', function (e) {
    forceSync();
  });

  deliverInput.on('select2:select', function (e) {
    forceSync();
  });

  initializeInputs({
    lobInput: lobInput,
    eventsInput: eventsInput,
    venueFiltersInput: venueFiltersInput,
    venuesInput: venuesInput,
    deliverInput: deliverInput
  });
});