var express = require('express');
var router = express.Router();

var placeholderData = [
  {
      id: 0,
      text: 'enhancement'
  },
  {
      id: 1,
      text: 'bug'
  },
  {
      id: 2,
      text: 'duplicate'
  },
  {
      id: 3,
      text: 'invalid'
  },
  {
      id: 4,
      text: 'wontfix'
  }
];

/* GET initial inputs listing. */
router.get('/', function(req, res, next) {
  return res.send(placeholderData);
});

/* GET initial inputs listing. */
router.get('/options', function(req, res, next) {
  const optionsResponse = {
    lob: [
      {
        id: 'marketing',
        value: 'Venue Experience',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
      {
        id: 'food',
        value: 'Food & Beverage',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
      {
        id: 'special',
        value: 'Special Events',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
      {
        id: 'premium',
        value: 'Premium',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
      {
        id: 'brands',
        value: 'Brands',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
      {
        id: 'operations',
        value: 'Operations',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
      {
        id: 'other',
        value: 'All Other',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
    ],
    events: [
      {
        id: 'blood-red-shoes',
        value: 'Blood Red Shoes',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
      {
        id: 'chelsea-grin-with-carnifex',
        value: 'Chelsea Grin with Carnifex',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
      {
        id: 'club-90s-presents-one-direction',
        value: 'Club 90s Presents Midnight Memories One Direction Night',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
      {
        id: 'foreigner',
        value: 'Foreigner',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
    ],
    venueFilters: [
      {
        id: 'venue',
        value: 'ALL VENUES',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
      {
        id: 'amp',
        value: 'ALL AMPS',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
      {
        id: 'club-theater',
        value: 'ALL CLUB / THEATER',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
      {
        id: 'hob',
        value: 'ALL HOB',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
    ],
    venues: [
      // doesnt show when AMP or HOB selected
      {
        id: '191-toole-tucson',
        value: '191 Toole (Tucson) - IS A CLUB/THEATER',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [
            'amp',
            'hob'
          ],
          venues: []
        }
      },
      // doesnt show when club/theater or HOB selected
      {
        id: '1st-bank-center-broomfield',
        value: '1st Bank Center (Broomfield) - IS AN AMP',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [
            'club-theater',
            'hob'
          ],
          venues: []
        }
      },
      // doesnt show when HOB or AMP selected
      {
        id: 'abraham-chavez-theatre-el-paso)',
        value: 'Abraham Chavez Theatre (El Paso) - NO FOOD LOB, IS A CLUB/THEATER',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [ 
            'food' 
          ],
          venueFilters: [
            'amp',
            'hob'
          ],
          venues: []
        }
      },
      // doesnt show when club/theater or AMP selected
      {
        id: 'amalie-arena-tampa',
        value: 'Amalie Arena (Tampa) - IS NOT A CLUB/THEATER OR AMP',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [
            'club-theater',
            'amp'
          ],
          venues: []
        }
      },
    ],
    deliverTo: [
      // always shows up
      {
        id: 'deliver-accounts',
        value: 'Accounts',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },
      // always shows up
      {
        id: 'deliver-ap',
        value: 'ALL AP',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: []
        }
      },

      // in this scenario Kimmy Adamson never
      // works at `1st-bank-center-broomfield`
      {
        id: 'deliver-kimmy-adamson',
        value: 'Kimmy Adamson - (DOES NOT WORK WITH VENUE 1st Bank Center Broomfield)',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [],
          venueFilters: [],
          venues: [
            '1st-bank-center-broomfield'
          ]
        }
      },
      // in this scenario Joy Alba only does 
      // `food` and `other` LOBs
      {
        id: 'deliver-joy-alba',
        value: 'Joy Alba - (only does FOOD and OTHER LOBs)',
        removeWhen: {
          deliverTo: [],
          events: [],
          lob: [ 
            'marketing',
            'special',
            'premium',
            'brands',
          ],
          venueFilters: [],
          venues: []
        }
      },
    ],
  };
  return res.send(optionsResponse);
});

/* GET users listing. */
router.get('/lob/:id', function(req, res, next) {
  if (req.params.id) {
    // do something
    return res.send([])
  }
  return res.send(placeholderData);
});

module.exports = router;
